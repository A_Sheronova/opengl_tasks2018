#include <string>
#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "Shader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "SOIL.h"

#define SIZE 0.5f
#define SCALE 1000

#define COUNTX 600
#define COUNTZ 600
#define VERTEX_COUNT COUNTX*COUNTZ

GLuint screenWidth = 800, screenHeight = 600;


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void Do_Movement();
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

void genTexture(const char* path, GLuint &tex);

glm::vec3 getCross(int, int, int, int, int, int);

glm::vec3 cameraPos   = glm::vec3(-210.0f, 0.2f,  210.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.2f, 10.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);


GLfloat vertices[VERTEX_COUNT * 3];


GLfloat yAngle   = -90.0f;	
GLfloat xAngle =   0.0f;
GLfloat X =  screenWidth  / 2.0;
GLfloat Y =  screenHeight / 2.0;
GLfloat curHeight = 0;

bool keys[1024];

int main()
{
    	glfwInit();
    	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    	glfwWindowHint(GLFW_SAMPLES, 4);

    	GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Relief", nullptr, nullptr); 
	glfwMakeContextCurrent(window);

	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	

        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glewExperimental = GL_TRUE;
	glewInit();

	glViewport(0, 0, screenWidth, screenHeight);

	glEnable(GL_DEPTH_TEST);
	
	Shader ourShader("491SheronovaData/shaders/shader.vert", "491SheronovaData/shaders/shader.frag");

         int width, height;
         unsigned char* image = SOIL_load_image("491SheronovaData/height.jpg", &width, &height, 0, SOIL_LOAD_RGB);
       

	float posX, posZ;
	posZ = -SIZE / 2;
	for (int z = 0; z < COUNTZ; z++)
	{
		posX = -SIZE / 2;
		for (int x = 0; x < COUNTX; x++)
		{
			vertices[3 * x + z * COUNTX * 3] = posX;
			vertices[3 * x + z * COUNTX * 3 + 1] = ((float)image[(z + x  * width) * 3]) / 255 * 0.05;
			vertices[3 * x + z * COUNTX * 3 + 2] = posZ;
			posX += (SIZE / (COUNTX - 1));
		}
		posZ += (SIZE / (COUNTX - 1));
	}
	SOIL_free_image_data(image);	

	GLuint indices_count = (COUNTX - 1) * (COUNTZ - 1) * 2 * 3;

	GLfloat norm_indices[VERTEX_COUNT * 3];

	int i = 0; 
	for (int z = 0; z < COUNTZ; z++)
	{
		for (int x = 0; x < COUNTX; x++) 
		{
			glm::vec3 n1 = getCross(x, z, x, z - 1, x + 1, z - 1);
			glm::vec3 n2 = getCross(x, z, z + 1, z - 1, x + 1, z);
			glm::vec3 n3 = getCross(x, z, x + 1, z, x, z + 1);
			glm::vec3 n4 = getCross(x, z, x, z + 1, x - 1, z + 1);
			glm::vec3 n5 = getCross(x, z, x - 1, z + 1, x - 1, z);
			glm::vec3 n6 = getCross(x, z, x - 1, z, x, z - 1);
			glm::vec3 norm = (n1 + n2 + n3 + n4 + n5 + n6) / (float)6.0;
			norm_indices[i++] = norm.x;
			norm_indices[i++] = norm.y;
			norm_indices[i++] = norm.z;
			continue;
		}
	}

	GLuint* indices = new GLuint[indices_count];
        int j = 0;
        for (int x = 0; x < COUNTX - 1; ++x)
        {
                for (int z = 0; z < COUNTZ - 1; ++z)
                {
                        indices[j++] = x + z * COUNTX;
                        indices[j++] = x + (z + 1) * COUNTX;
                        indices[j++] = (x + 1) + z * COUNTX;
                        indices[j++] = x + (z + 1) * COUNTX;
                        indices[j++] = (x + 1) + (z + 1) * COUNTX;
                        indices[j++] = (x + 1) + z * COUNTX;
                }
        }

	GLuint VBO, VAO, VBOnorm, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &VBOnorm);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

    	glBindBuffer(GL_ARRAY_BUFFER, VBO);
    	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices_count, indices, GL_STATIC_DRAW);
	delete indices;

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    	glEnableVertexAttribArray(0);

    	glBindBuffer(GL_ARRAY_BUFFER, VBOnorm);
   	glBufferData(GL_ARRAY_BUFFER, sizeof(norm_indices), norm_indices, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);

	
	GLuint tex_grass, tex_snow, tex_sand, tex_gravel, tex_mask;
	genTexture("491SheronovaData/grass.jpg", tex_grass);
	genTexture("491SheronovaData/gravel.jpg", tex_gravel);
	genTexture("491SheronovaData/snow.jpg", tex_snow);
	genTexture("491SheronovaData/sand.jpg", tex_sand);
	genTexture("491SheronovaData/mask2.png", tex_mask);
	
	while(!glfwWindowShouldClose(window))
    	{
        	glfwPollEvents();
		Do_Movement();

        	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  
        	ourShader.Use();
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tex_grass);
		glUniform1i(glGetUniformLocation(ourShader.Program, "tex_grass"), 0);	

		glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, tex_snow);
                glUniform1i(glGetUniformLocation(ourShader.Program, "tex_snow"), 1);  
		
		glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_2D, tex_gravel);
                glUniform1i(glGetUniformLocation(ourShader.Program, "tex_gravel"), 2);  

		glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_2D, tex_sand);
                glUniform1i(glGetUniformLocation(ourShader.Program, "tex_sand"), 3);  

		glActiveTexture(GL_TEXTURE4);
                glBindTexture(GL_TEXTURE_2D, tex_mask);
                glUniform1i(glGetUniformLocation(ourShader.Program, "tex_mask"), 4);  	
		

		ourShader.Set3V("lightColor", 1.0f, 1.0f, 1.0f);
		ourShader.Set3V("light.diffuse", 0.6f, 0.6f, 0.6f);
		ourShader.Set3V("light.ambient", 0.5f, 0.5f, 0.5f);
		ourShader.Set3V("light.specular", 0.2f, 0.2f, 0.2f);
		glm::vec3 light = glm::normalize(glm::vec3(-10.0f, 10.0f, -10.0f));
		ourShader.Set3V("light.direction", light.x, light.y, light.z);
		ourShader.Set3V("viewPos", cameraPos.x, cameraPos.y, cameraPos.z);
		ourShader.Set4V("specular", 0.3, 0.1, 0.2, 0.4);
		ourShader.SetVec1f("size", SIZE);
        	glm::mat4 model;
		model = glm::rotate(model, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
          	model = glm::scale(model, glm::vec3(SCALE));   
		glm::mat4 view;
        	view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        	glm::mat4 projection;	
        	projection = glm::perspective(45.0f, (float)screenWidth/(float)screenHeight, 0.1f, 1000.0f);
        	GLint modelLoc = glGetUniformLocation(ourShader.Program, "model");
        	GLint viewLoc = glGetUniformLocation(ourShader.Program, "view");
        	GLint projLoc = glGetUniformLocation(ourShader.Program, "projection");
		GLint normLoc = glGetUniformLocation(ourShader.Program, "normal_matrix");
        	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix3fv(normLoc, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(view * model)))));
	      	glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, indices_count, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
  
	      	glfwSwapBuffers(window);
    	}
    	glDeleteVertexArrays(1, &VAO);
    	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &VBOnorm);
	glDeleteBuffers(1, &EBO);
    	glfwTerminate();
    	return 0;
}

void Do_Movement()
{ 
	GLfloat cameraSpeed = 1.0f;
    	if (keys[GLFW_KEY_W])
        	cameraPos += cameraSpeed * glm::normalize(cameraFront);
    	if (keys[GLFW_KEY_S])
        	cameraPos -= cameraSpeed * glm::normalize(cameraFront);
    	if (keys[GLFW_KEY_A])
        	cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    	if (keys[GLFW_KEY_D])
        	cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
   	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        	glfwSetWindowShouldClose(window, GL_TRUE);
    	if (key >= 0 && key < 1024)
    	{
        	if (action == GLFW_PRESS)
            		keys[key] = true;
        	else if (action == GLFW_RELEASE)
            		keys[key] = false;
    	}
}

void mouse_callback(GLFWwindow* window, double newX, double newY)
{
    	xAngle += (newX - X) * 0.1f;
    	yAngle += (Y - newY) * 0.1f;
    	X = newX;
    	Y = newY;
	
    	if(yAngle > 89.9f)
    		yAngle = 89.9f;
    	if(yAngle < -89.9f)
        	yAngle = -89.9f;

    	glm::vec3 front;
    	front.x = cos(glm::radians(xAngle)) * cos(glm::radians(yAngle));
    	front.y = sin(glm::radians(yAngle));
    	front.z = sin(glm::radians(xAngle)) * cos(glm::radians(yAngle));
    	cameraFront = glm::normalize(front);
}

void genTexture(const char* path, GLuint &tex) 
{
	int width, height;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
  
    	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    	unsigned char* image = SOIL_load_image(path, &width, &height, 0, SOIL_LOAD_RGB);
    	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    	glGenerateMipmap(GL_TEXTURE_2D);
    	SOIL_free_image_data(image);
    	glBindTexture(GL_TEXTURE_2D, 0); 
}

glm::vec3 getCross(int ax, int az, int bx, int bz, int cx, int cz)
{
	ax = (ax  + COUNTX) % COUNTX;
	bx = (bx + COUNTX) % COUNTX;
	cx = (cx + COUNTX) % COUNTX;
	az = (az + COUNTZ) % COUNTZ;
	bz = (bz + COUNTZ) % COUNTZ;
	cz = (cz + COUNTZ) % COUNTZ;
	glm::vec3 v1 = glm::vec3(vertices[3 * ax + 3 * COUNTX * az],
				vertices[3 * ax + 3 * COUNTX * az + 1],
				vertices[3 * ax + 3 * COUNTX * az + 2]);
	glm::vec3 v2 = glm::vec3(vertices[3 * bx + 3 * COUNTX * bz],
                                vertices[3 * bx + 3 * COUNTX * bz + 1],
                                vertices[3 * bx + 3 * COUNTX * bz + 2]);
	glm::vec3 v3 = glm::vec3(vertices[3 * cx + 3 * COUNTX * cz],
                                vertices[3 * cx + 3 * COUNTX * cz + 1],
                                vertices[3 * cx + 3 * COUNTX * cz + 2]);
	glm::vec3 norm1 = glm::normalize(glm::cross(v3 - v2, v1 - v2));
	
	return norm1;
}	
