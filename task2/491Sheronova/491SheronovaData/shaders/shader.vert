#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;


out vec3 FragPos;
out vec2 TexCoord;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat3 normal_matrix;
uniform float size;


void main()
{
        TexCoord = position.xz * vec2(1/size, -1/size) + vec2(0.5, -0.5);
	Normal = normal_matrix * normal;
	vec4 Position = projection * view * model * vec4(position, 1.0f);
	FragPos = vec3(Position.xyz);
	gl_Position = Position;
}


