#version 330 core
in vec3 FragPos;
in vec2 TexCoord;
in vec3 Normal;

out vec4 color;


uniform sampler2D tex_grass;
uniform sampler2D tex_gravel;
uniform sampler2D tex_snow;
uniform sampler2D tex_sand;
uniform sampler2D tex_mask;

uniform vec3 lightColor;
uniform vec3 viewPos;

struct Light {
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

uniform vec4 specular;

uniform Light light;
void main()
{
	float ambientStrength = 0.5f;
	vec3 ambient =  light.ambient * ambientStrength * lightColor;

	
	vec3 norm = normalize(Normal);
	vec3 diffuse =  light.diffuse * max(dot(norm, -light.direction), 0.0) * lightColor;

	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(light.direction, norm);

	vec3 spec =light.specular * pow(max(dot(viewDir, reflectDir), 0.0), 32) * lightColor;
	vec3 specular_grass = spec * specular.x;
	vec3 specular_gravel =  spec * specular.y;
	vec3 specular_snow =  spec * specular.w;
	vec3 specular_sand =  spec * specular.z;

	vec4 mask = texture(tex_mask, TexCoord);
	vec4 grass = texture(tex_grass, TexCoord);
	vec4 gravel = texture(tex_gravel, TexCoord);
	vec4 snow = texture(tex_snow, TexCoord);
	vec4 sand = texture(tex_sand, TexCoord);	
	color = vec4(mask.a * snow.xyz * (diffuse + specular_snow + ambient) + mask.r * grass.xyz * (diffuse + specular_grass + ambient) + mask.g * gravel.xyz * (diffuse + specular_gravel + ambient) + mask.b * sand.xyz * (diffuse + specular_sand + ambient), 1.0f);

}
